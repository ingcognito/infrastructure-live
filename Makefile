SHELL := /bin/bash -e

.PHONY: *

# Add the following 'help' target to your Makefile
# And add help text after each target name starting with '\#\#'
#

help: ## This help message
	@echo -e "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\x1b[36m\1\\x1b[m:\2/' | column -c2 -t -s :)"

gcp-service-account:	## Creates GCP Terraform Service account and copies key to Terraform
	./scripts/service_account_remote_storage.sh && cp ./terraform-sa-keyfile.json


